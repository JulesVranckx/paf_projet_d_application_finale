module banc_de_registres_tb ();
        logic clk;
        logic  reset_n;
        logic [4:0] RS1_add;
        logic [4:0] RS2_add;
        logic [4:0] RD_add;
        logic Load_Rd;
		logic Load_MEM;
		logic Store_PC;
		logic [31:0] ALU_Out;
		logic [31:0] d_data;
		logic [31:0] PC;
        logic [31:0] RS1;
        logic [31:0] RS2;
        logic [31:0] RD;

        banc_de_registres banc_test(.clk(clk), .reset_n(reset_n), .RS1_add(RS1_add),
                                    .RS2_add(RS2_add), .RD_add(RD_add), .Load_Rd(Load_Rd),
                                    .Load_MEM(Load_MEM), .Store_PC(Store_PC), .ALU_Out(ALU_Out),
                                    .d_data(d_data), .PC(PC),
                                    .RS1(RS1), .RS2(RS2), .RD(RD));
        always begin
            # 5ns
            clk <= !clk;
        end

        initial begin
            clk <= 0;
            reset_n <= 0;
            RS1_add <= 0;
            RS2_add <= 0;
            RD_add <= 5;
            Load_Rd <= 0;
            Load_MEM <= 0;
            Store_PC <= 0;

            d_data <= 32'b10000;
            PC <= 32'b1010;
            ALU_Out <= 32'b100;
            # 10ns
            reset_n <= 1;
            # 15ns
            Load_Rd <= 1;
            # 10ns
            Load_Rd <= 0;
            RS1_add <= 5;
            RD_add <= 6;
            ALU_Out <= 32'b111;
            # 10ns
            Load_Rd <= 1;
            # 10ns
            Load_Rd <= 0;
        end
endmodule // banc_de_registres_tb
