module ALU
		(
	   input logic [31:0] RS1,
	   input logic [31:0] RS2,
	   input logic signed [31:0] Immediate,
	   input logic [5:0] Op_code,
		 input logic R_instr,
	   output logic signed [31:0] OUT
		 );


		 // Définition du jeu d'instructions
		 localparam ADD = 6'h20;
         localparam ADDI = 6'h08;
         localparam AND = 6'h24;
         localparam ANDI = 6'h0c;
         //localparam BEQZ = 6'h04;
         localparam BNEZ = 6'h05;
         localparam J = 6'h02;
         localparam JAL = 6'h03;
         localparam JALR = 6'h13;
         localparam JR = 6'h12;
         localparam LHI = 6'h0f;
         localparam LW = 6'h23;
         localparam OR = 6'h25;
         localparam ORI = 6'h0d;
         localparam SEQ = 6'h28;
         localparam SEQI = 6'h18;
         localparam SLE = 6'h2c;
         localparam SLEI = 6'h1c;
         //localparam SLL = 6'h04;
         localparam SLLI = 6'h14;
         localparam SLT = 6'h2a;
         localparam SLTI = 6'h1a;
         localparam SNE = 6'h29;
         localparam SNEI = 6'h19;
         localparam SRA = 6'h07;
         localparam SRAI = 6'h17;
         localparam SRL = 6'h06;
         localparam SRLI = 6'h16;
         localparam SUB = 6'h22;
         localparam SUBI = 6'h0a;
         localparam SW = 6'h2b;
         localparam XOR = 6'h26;
         localparam XORI = 6'h0e;

		 // ALU

		 always @(*)

		 		case(Op_code)

					// Add
					ADD : OUT <= RS1 + RS2;

					//Add Immediate
					ADDI : OUT <= RS1 + Immediate;

					//And
					AND : OUT <= RS1 && RS2;

					//And Immediate
					ANDI : OUT <= RS1 && $unsigned(Immediate) ;

					6'h04 : if (R_instr)
											//Branch if equal to zero
											if (RS1 == 32'b0)
													OUT <= Immediate;
								 			else
								 					OUT <= 0;
									else
											//Shift left logical
											SLL : OUT <= RS1 << RS2[2:0];


					//Branch if not equel to zero
					BNEZ : if (RS1 == 32'b0)
									OUT <= 0;
								 else
								 	OUT <= Immediate;

					//Jump
					J : OUT <= Immediate;

					//Jump and link
					JAL : OUT <= Immediate;

					//Jump and link register
					JALR : OUT <= RS1;

					//Jump register
					JR : OUT <= RS1;

					//Load high bits
					LHI :  OUT <= {$unsigned(Immediate[15:0]), 16'd0};

					// Load word
					LW : OUT <= RS1 + Immediate;

					//OR
					OR : OUT <= RS1 | RS2;

					//OR Immediate
					ORI : OUT <= RS1 | $unsigned(Immediate);

					//Set if equal
					SEQ : OUT <= (RS1 == RS2);

					//Set if equal to Immediate
					SEQI : OUT <= (RS1 == Immediate);

					//Set if less than or equal
					SLE : OUT <=  (RS1 <= RS2);

					//Set if less than or equal to Immediate
					SLEI : OUT <=  (RS1 <= Immediate);

					//Shift left logical Immediate
					SLLI : OUT <= RS1 << Immediate[2:0];

					//Set if less than
					SLT : OUT <= (RS1 < RS2);

					//Set if less than Immediate
					SLTI : OUT <= (RS1 < Immediate);

					//Set if not equal
					SNE : OUT <= (RS1 != RS2);

					//Set if not equal to Immediate
					SNEI : OUT <= (RS1 != Immediate);

					//Shift right arithmetic
					SRA : OUT <= RS1 >>> RS2[2:0];

					//Shift right arithmetic Immediate
					SRAI : OUT <= RS1 >>> Immediate[2:0];

					//Shit right logical
					SRL : OUT <= RS1 >> RS2[2:0];

					//Shift right logical Immediate
					SRLI : OUT <= RS1 >> $unsigned(Immediate[2:0]);

					//Substract
					SUB : OUT <= RS1 - RS2;

					//Substract to Immediate
					SUBI : OUT <= RS1 - Immediate;

					//Store word
					SW : OUT <= RS1 + Immediate;

					//Exclusive OR
					XOR : OUT <= RS1 ^ RS2;

					//Exclusive OR Immediate
					XORI : OUT <= RS1 ^ $unsigned(Immediate);

					default:
						OUT <= 32'h0;
				endcase

endmodule //ALU
