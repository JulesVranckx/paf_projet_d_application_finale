#########################################################################################################################
#########################################################################################################################
################################### Transformation assembleur en langage machine ########################################
#########################################################################################################################
#########################################################################################################################

import sys

class Instruction:
    """docstring for Instruction."""

    def __init__(self, name, Op_code, label, address, type, operandes):
        self.name = name
        self.Op_code = Op_code
        self.label = label
        self.address = address
        self.type = type
        self.operandes = operandes


#########################################################################################################################
################################### Ouverture du fichier ################################################################
#########################################################################################################################
cmd = sys.argv
data = cmd[1]


######################Open file########################

import re

def preformat_line(line):
    """Permet d'ecrire avec des espaces entre les differents caracteres"""
    ret = line.lstrip()
    ret = ret.rstrip()
    r_spaces = re.compile('\s+')
    r_label = re.compile('\s*:\s*')
    r_args = re.compile('\s*,\s*')
    ret = r_spaces.sub(' ', ret)
    ret = r_label.sub(':', ret)
    ret = r_args.sub(',', ret)
    return ret

def get_type(I_name):
    """Renvoie le type de l'instruction"""
    if I_name == "ADD" :
        ret = 'R'
    elif I_name == "ADDI" :
        ret = 'I'
    elif I_name == "AND" :
        ret = 'R'
    elif I_name == "ANDI" :
        ret = 'I'
    elif I_name == "BEQZ" :
        ret = 'I'
    elif I_name == "BNEZ" :
        ret = 'I'
    elif I_name == "J" :
        ret = 'J'
    elif I_name == "JAL" :
        ret = 'J'
    elif I_name == "JALR" :
        ret = 'I'
    elif I_name == "JR" :
        ret = 'I'
    elif I_name == "LHI" :
        ret = 'I'
    elif I_name == "LW" :
        ret = 'I'
    elif I_name == "OR" :
        ret = 'R'
    elif I_name == "ORI" :
        ret = 'I'
    elif I_name == "SEQ" :
        ret = 'R'
    elif I_name == "SEQI" :
        ret = 'I'
    elif I_name == "SLE" :
        ret = 'R'
    elif I_name == "SLEI" :
        ret = 'I'
    elif I_name == "SLL" :
        ret = 'R'
    elif I_name == "SLLI" :
        ret = 'I'
    elif I_name == "SLT" :
        ret = 'R'
    elif I_name == "SLTI" :
        ret = 'I'
    elif I_name == "SNE" :
        ret = 'R'
    elif I_name == "SNEI" :
        ret = 'I'
    elif I_name == "SRA" :
        ret = 'R'
    elif I_name == "SRAI" :
        ret = 'I'
    elif I_name == "SRL" :
        ret = 'R'
    elif I_name == "SRLI" :
        ret = 'I'
    elif I_name == "SUB" :
        ret = 'R'
    elif I_name == "SUBI" :
        ret = 'I'
    elif I_name == "SW" :
        ret = 'I'
    elif I_name == "XOR" :
        ret = 'R'
    elif I_name == "XORI" :
        ret = 'I'
    else :
        print("Fatal Error :\nUnknown instruction")
        return None
    return ret

def get_op_code(I_name):
    """Renvoie la valeur hexadecimale de l'Op_code"""
    if I_name == "ADD" :
        ret = 0x20
    elif I_name == "ADDI" :
        ret = 0x08
    elif I_name == "AND" :
        ret = 0x24
    elif I_name == "ANDI" :
        ret = 0x0c
    elif I_name == "BEQZ" :
        ret = 0x04
    elif I_name == "BNEZ" :
        ret = 0x05
    elif I_name == "J" :
        ret = 0x02
    elif I_name == "JAL" :
        ret = 0x03
    elif I_name == "JALR" :
        ret = 0x13
    elif I_name == "JR" :
        ret = 0x12
    elif I_name == "LHI" :
        ret = 0x0f
    elif I_name == "LW" :
        ret = 0x23
    elif I_name == "OR" :
        ret = 0x25
    elif I_name == "ORI" :
        ret = 0x0d
    elif I_name == "SEQ" :
        ret = 0x28
    elif I_name == "SEQI" :
        ret = 0x18
    elif I_name == "SLE" :
        ret = 0x2c
    elif I_name == "SLEI" :
        ret = 0x1c
    elif I_name == "SLL" :
        ret = 0x04
    elif I_name == "SLLI" :
        ret = 0x14
    elif I_name == "SLT" :
        ret = 0x2a
    elif I_name == "SLTI" :
        ret = 0x1a
    elif I_name == "SNE" :
        ret = 0x29
    elif I_name == "SNEI" :
        ret = 0x19
    elif I_name == "SRA" :
        ret = 0x07
    elif I_name == "SRAI" :
        ret = 0x17
    elif I_name == "SRL" :
        ret = 0x06
    elif I_name == "SRLI" :
        ret = 0x16
    elif I_name == "SUB" :
        ret = 0x22
    elif I_name == "SUBI" :
        ret = 0x0a
    elif I_name == "SW" :
        ret = 0x2b
    elif I_name == "XOR" :
        ret = 0x26
    elif I_name == "XORI" :
        ret = 0x0e
    else :
        print("Fatal Error :\nUnknown instruction")
        return None
    return ret

def open_file(data):
    """Ouvre le ficher 'data' et renvoie un tableau de tableau, comprenant chacun une ligne de code, ainsi que le nombre de lignes"""
    lines = ""
    instructions = []
    current_address = 0
    label_list = {}

    with open(data) as f :
    	for data_line in f:
            if data_line!="\n":
                lines = lines + data_line
    lines = lines[:-1]
    lines = lines.split("\n")
    for line in lines :
        line = preformat_line(line)
        temp = line.split(":")
        if len(temp)==2:
            current_label = temp[0]
            temp = temp[1]
            label_list[current_label] = current_address
        else:
            current_label=""
            temp = temp[0]
        temp = temp.split(" ")
        temp[1] = temp[1].split(",")
        for num in range(len(temp[1])):
            if(temp[1][num][0]=='$'):
                temp[1][num] = temp[1][num][1:]
            elif (temp[1][num][:2] == "0x" or temp[1][num][:2] == "0h"):
                temp[1][num] = int(temp[1][num][2:], 16)
            elif (temp[1][num][:2] == "0b"):
                temp[1][num] = int(temp[1][num][2:], 2)

        instructions.append(Instruction(temp[0], get_op_code(temp[0]), current_label, current_address, get_type(temp[0]), temp[1]))
        current_address+=4;
    for i in instructions :
        if i.type == 'J' :
            i.operandes[0] = int(label_list[i.operandes[0]]) - i.address
        elif i.name == "BNEZ" or i.name == "BEQZ":
            i.operandes[1] = int(label_list[i.operandes[1]]) - i.address
        for j in range(len(i.operandes)) :
            i.operandes[j] = int(i.operandes[j])
        print(i.operandes)


    return instructions

def decrypt(i):
    """Give the hexadecimal equivalent of i"""
    if i.type == 'R':
        ret = hex( (i.operandes[1] << 21) | (i.operandes[2] << 16) | (i.operandes[0] << 11) | i.Op_code)
    elif i.type == 'J' :
        ret = hex((i.Op_code << 26) | (i.operandes[0] & 0x3ffffff))
    elif i.type == 'I':
        if i.name == "JR" or i.name == "JALR":
            ret = hex( (i.Op_code << 26) | (i.operandes[0] << 21))
        elif i.name == "BEQZ" or i.name == "BNEZ":
            ret = hex( (i.Op_code << 26) | (i.operandes[0] << 21) | (i.operandes[1] & 0xffff))
        elif i.name == "LHI":
            ret = hex( (i.Op_code << 26) | (i.operandes[0] << 16) | i.operandes[1])
        elif i.name == "ANDI" or i.name == "ORI" or i.name == "XORI" or i.name == "SLLI" or i.name == "SRAI" or i.name == "SRLI" :
            ret = hex( (i.Op_code << 26) | (i.operandes[1] << 21) | (i.operandes[0] << 16) | i.operandes[2])
        else :
            ret = hex( (i.Op_code << 26) | (i.operandes[1] << 21) | (i.operandes[0] << 16) | (i.operandes[2] & 0xffff))
    return ret[2:].zfill(8)

def parse(input):
    """Parse les donnee en entree"""
    ret = ""
    instructions = open_file(input)
    for instruction in instructions :
        ret = ret + decrypt(instruction) + "\n"
    return ret

with open(cmd[2], "w") as f :
    f.write(parse(data))
