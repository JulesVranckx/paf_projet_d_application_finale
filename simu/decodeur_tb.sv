module decodeur_tb ();

    logic clk = 0;
    logic reset_n;
    logic Load_I;
    logic [31:0] i_data_read;
    logic [5:0] Op_code;
    logic [31:0] Immediate;
    logic [4:0] RS1_add;
    logic [4:0] RS2_add;
    logic [4:0] Rd_add;


      decodeur decodeur(.clk(clk), .reset_n(reset_n), .Load_I(Load_I), .i_data_read(i_data_read), .Op_code(Op_code), .Immediate(Immediate),
                            .RS1_add(RS1_add), .RS2_add(RS2_add), .Rd_add(Rd_add)) ;


      always

        #5ns clk <= !clk ;

      initial begin

        reset_n <= 0 ;
        Load_I <= 1 ;
        i_data_read <= 32'b0000000001000100001100000000010;


        #11ns reset_n <= 1 ;

        #11ns
        i_data_read <= 32'b00110100000000010000000000000001;  // ORI  $1,$0,1
        #11ns
        i_data_read <= 32'b00000000000000010001000000100101;  // OR   $2,$0,$1
                                                              // boucle
        #11ns
        i_data_read <= 32'b00110100000000110000000000001010;  // ORi_data_read  $3,$0,10
        #11ns
        i_data_read <= 32'b00000000001000100010000000100000;  // ADD  $4,$1,$2
        #11ns
        i_data_read <= 32'b00000000000000100000100000100101;  // OR   $1,$0,$2
        #11ns
        i_data_read <= 32'b00000000000001000001000000100101;  // OR   $2,$0,$4
        #11ns
        i_data_read <= 32'b00100000011000111111111111111111;  // ADDi_data_read $3,$3,-1
        #11ns
        i_data_read <= 32'b00010100011000001111111111110000;  // BNEZ $3,boucle
        #11ns
                                                    // fin:
        i_data_read <= 32'b00001000000000000000000000000000;


      end


endmodule // decodeur_tb
