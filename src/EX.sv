module EX
  (
    input logic clk,
    input logic reset_n,
    input signed [31:0] RS1,
    input signed [31:0] RS2,
    input logic [31:0] RD,
    input signed [31:0] Immediate_ID,
    input logic [5:0] Op_code_ID,
    input logic [4:0] RD_add_ID,
    input logic Load_RD_ID,
    input logic Load_MEM_ID,
    input logic Write_MEM_ID,
    input logic Add_PC_ID,
		input logic R_instr,

    input signed [31:0] OUT_MEM,
    input signed [31:0] OUT_WB,
    input logic [4:0] RD_add_MEM,
    input logic [4:0] RD_in_add,
    input logic Load_RD_MEM,
    input logic Load_RD_WB,

    input logic [4:0] RS1_add,
    input logic [4:0] RS2_add,
    output logic [31:0] OUT_EX,
    output logic [4:0] RD_add_EX,
    output logic Load_RD_EX,
    output logic Load_MEM_EX,
    output logic Write_MEM_EX,
    output logic [31:0]  RD_RAM_EX,
    output logic Add_PC_EX,
    output logic Load_PC,

    input logic [31:0] i_addr_ID,
    output logic [31:0] i_addr_EX
    );

    // Définition du jeu d'instructions
    localparam ADD = 6'h20;
    localparam ADDI = 6'h08;
    localparam AND = 6'h24;
    localparam ANDI = 6'h0c;
    localparam BEQZ = 6'h04;
    localparam BNEZ = 6'h05;
    localparam J = 6'h02;
    localparam JAL = 6'h03;
    localparam JALR = 6'h13;
    localparam JR = 6'h12;
    localparam LHI = 6'h0f;
    localparam LW = 6'h23;
    localparam OR = 6'h25;
    localparam ORI = 6'h0d;
    localparam SEQ = 6'h28;
    localparam SEQI = 6'h18;
    localparam SLE = 6'h2c;
    localparam SLEI = 6'h1c;
    localparam SLL = 6'h04;
    localparam SLLI = 6'h14;
    localparam SLT = 6'h2a;
    localparam SLTI = 6'h1a;
    localparam SNE = 6'h29;
    localparam SNEI = 6'h19;
    localparam SRA = 6'h07;
    localparam SRAI = 6'h17;
    localparam SRL = 6'h06;
    localparam SRLI = 6'h16;
    localparam SUB = 6'h22;
    localparam SUBI = 6'h0a;
    localparam SW = 6'h2b;
    localparam XOR = 6'h26;
    localparam XORI = 6'h0e;

    logic [5:0] Op_code_EX ;
    logic [31:0] Immediate_EX ;
		logic R_instr_EX ;

    logic signed [31:0] ALU_in_1;
    logic signed [31:0] ALU_in_2;

    logic [4:0] RS1_add_EX;
    logic [4:0] RS2_add_EX;

    always @ ( posedge clk or negedge reset_n )
    begin
      if (!reset_n)
        begin
          Load_RD_EX <= 0 ;
          Load_MEM_EX <=0 ;
          Write_MEM_EX <= 0 ;
          RD_add_EX <= 0 ;
          RS1_add_EX <= 0;
          RS2_add_EX <= 0;
          Op_code_EX <= 0 ;
          Immediate_EX <= 0 ;
          Add_PC_EX <= 0;
					R_instr_EX <= 0 ;
        end
      else
        begin
          Load_RD_EX <= Load_RD_ID ;
          Load_MEM_EX <= Load_MEM_ID ;
          Write_MEM_EX <= Write_MEM_ID ;
          RD_add_EX <= RD_add_ID ;
          RS1_add_EX <= RS1_add;
          RS2_add_EX <= RS2_add;
          Op_code_EX <= Op_code_ID ;
          Immediate_EX <= Immediate_ID ;
          Add_PC_EX <= Add_PC_ID;
					R_instr_EX <= R_instr ;
        end
    end

    ALU alu(
            .RS1(ALU_in_1),
            .RS2(ALU_in_2),
            .Immediate(Immediate_EX),
						.R_instr(R_instr),
            .Op_code(Op_code_EX),
            .OUT(OUT_EX)
            );

    // Cas SW avec Data Forwarding
      always @(*) begin
            if ((RD_add_EX == RD_add_MEM) && (RD_add_MEM!=0) && (Load_RD_MEM)) begin
                RD_RAM_EX <= OUT_MEM;
            end
            else if ((RD_add_EX == RD_in_add) && (RD_in_add!=0) && (Load_RD_WB)) begin
                RD_RAM_EX <= OUT_WB;
            end
            else
                RD_RAM_EX <= RD;
        end

          always @ ( * ) begin
            Load_PC <= (Op_code_EX==BEQZ && OUT_EX==0) || (Op_code_EX==BNEZ && OUT_EX!=0) || (Op_code_EX==J)
            || (Op_code_EX==JAL) || (Op_code_EX==JALR) || (Op_code_EX==JR);
          end

      always @ ( * ) begin

            if ((RS1_add_EX == RD_add_MEM) && (RD_add_MEM!=0) && (Load_RD_MEM)) begin
                ALU_in_1 <= OUT_MEM;
            end
            else if ((RS1_add_EX == RD_in_add) && (RD_in_add!=0) && (Load_RD_WB)) begin
                ALU_in_1 <= OUT_WB;
            end
            else
                ALU_in_1 <= RS1;

            if ((RS2_add_EX == RD_add_MEM) && (RD_add_MEM!=0) && (Load_RD_MEM)) begin
              ALU_in_2 <= OUT_MEM;
            end
            else if ((RS2_add_EX == RD_in_add) && (RD_in_add!=0) && (Load_RD_WB)) begin
              ALU_in_2 <= OUT_WB;
            end
            else
              ALU_in_2 <= RS2;


      end


      always @ (posedge clk or negedge reset_n)
      begin
          if (!reset_n)
            i_addr_EX <= 0 ;
          else
            i_addr_EX <= i_addr_ID ;
      end



endmodule // EX
