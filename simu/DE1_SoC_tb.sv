module DE1_SoC_tb ();

    logic 	clock_50;

    logic [6:0] hex0;
    logic [6:0] hex1;
    logic [6:0] hex2;
    logic [6:0] hex3;
    logic [6:0] hex4;
    logic [6:0] hex5;

    logic [9:0] ledr;

    logic [3:0] key;


    DE1_SoC soc(.clock_50(clock_50), .hex0(hex0), .hex1(hex1), .hex2(hex2), .hex3(hex3), .hex4(hex4),
                .hex5(hex5), .key(key), .ledr(ledr), .sw(0));

    always begin
        # 5ns
        clock_50 <= !clock_50;
    end

    initial begin
        clock_50 <= 0;
        key <= 0;

        #20ns
        key <= 1;
    end

endmodule // DE1_SoC_tb
