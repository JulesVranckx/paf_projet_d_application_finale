module PC ( input logic clk,
            input logic reset_n,
            input logic Add_PC,
            input logic Inc_PC,
            input logic Load_PC,
            input logic [31:0] RS1,
            input logic signed [31:0] ALU_Out,
            output logic [31:0] Value
                );

    always @ (posedge clk or negedge reset_n) begin
        if(!reset_n)
            Value <= 0;
        else begin
            if (Load_PC) begin
                if(Add_PC)
                    Value <= Value + ALU_Out;
                else
                    Value <= RS1;
            end
            else if (Inc_PC) begin
                Value <= Value + 4;
            end
        end
    end
endmodule // PC
