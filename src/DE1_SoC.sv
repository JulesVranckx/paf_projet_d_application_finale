  module DE1_SoC
    #(parameter ROM_ADDR_WIDTH=10, parameter RAM_ADDR_WIDTH=10)
    (
     ///////// clock /////////
     input logic 	clock_50,

     ///////// hex  /////////
     output logic [6:0] hex0,
     output logic [6:0] hex1,
     output logic [6:0] hex2,
     output logic [6:0] hex3,
     output logic [6:0] hex4,
     output logic [6:0] hex5,

     ///////// key /////////
     input logic [3:0] 	key,

     ///////// ledr /////////
     output logic [9:0] ledr,

     ///////// œœ /////////
     input logic [9:0] 	sw
     );


   // Génération d'un reset à partir du bouton key[0]
   logic 			    reset_n;
   gene_reset gene_reset(.clk(clock_50), .key(key[0]), .reset_n(reset_n));

   // Instantication de la RAM pour les données
   logic [31:0] 		    ram_addr;
   logic [31:0] 		    ram_rdata, ram_wdata;
   logic 			    ram_we;
   logic 			    ram_rdata_valid;

   ram #(.ADDR_WIDTH(RAM_ADDR_WIDTH)) ram_data
     (
      .clk         ( clock_50                         ),
      .addr        ( ram_addr[(RAM_ADDR_WIDTH-1)+2:2] ),
      .we          ( ram_we                           ),
      .wdata       ( ram_wdata                        ),
      .rdata       ( ram_rdata                        ),
      .rdata_valid ( ram_rdata_valid                  )
      );

   // Instantication de la ROM pour les instructions
   logic [31:0] 		    rom_addr;
   logic [31:0] 		    rom_rdata;
   logic 			    rom_rdata_valid;

   rom #(.ADDR_WIDTH(ROM_ADDR_WIDTH)) rom_instructions
     (
      .clk         ( clock_50                         ),
      .addr        ( rom_addr[(ROM_ADDR_WIDTH-1)+2:2] ),
      .rdata       ( rom_rdata                        ),
      .rdata_valid ( rom_rdata_valid                  )
      );


   // Instanciation du processeur
   logic [31:0] d_address ;
   logic [31:0] d_rdata ;
   logic [31:0] d_wdata ;
   logic we ;

   DLX dlx
     (
      .clk            ( clock_50        ),
      .reset_n        ( reset_n         ),
      .d_address      ( d_address       ),
      .d_data_read    ( d_rdata         ),
      .d_data_write   ( d_wdata         ),
      .d_write_enable ( we              ),
      .d_data_valid   ( ram_rdata_valid ),
      .i_address      ( rom_addr        ),
      .i_data_read    ( rom_rdata       ),
      .i_data_valid   ( rom_rdata_valid )
      );

    // Instanciation du GPIO
    logic [31:0] gpio_address ;
    logic [31:0] gpio_rdata ;
    logic [31:0] gpio_wdata ;
    logic gpio_we ;

    GPIO gpio
      (
       .clk          ( clk           ),
       .reset_n      ( reset_n       ),
       .address      ( gpio_address  ),
       .data_write   ( gpio_wdata    ),
       .write_enable ( gpio_we       ),
       .sw           ( sw            ),
       .data_read    ( gpio_rdata    ),
       .ledr         ( ledr          )
        );


    // Connexions entre le dlx et la memoire (RAM et GPIO)

    // Fils entre d_wdata et ram_wdata et gpio_rdata
    always @(*)
      begin
      ram_wdata <= d_wdata ;
      gpio_wdata <= d_wdata ;
      end

    //Decodeur a partir de d_address
    logic sel_read ;
    logic RAM_CS ;
    logic GPIO_CS ;

    always @(*)
      if (d_address[31] == 0) // On adresse la RAM
        begin
        ram_addr <= d_address[30:0] ;
        gpio_address <= 0 ;
        RAM_CS <= 1 ;
        GPIO_CS <= 0 ;
        end
      else // On adresse le GPIO
        begin
        ram_addr <= 0 ;
        gpio_address <= d_address[30:0] ;
        GPIO_CS <= 1;
        RAM_CS <= 0 ;
        end

    // Multiplexeur pour d_rdata
    always @(*)
      if (RAM_CS)
        d_rdata <= ram_rdata ;
      else
       d_rdata <= gpio_rdata ;

   // gpio_we
   always @(*)
    gpio_we <= ( we && GPIO_CS) ;


  // ram_we
  always @(*)
    ram_we <= ( we && RAM_CS) ;

endmodule
