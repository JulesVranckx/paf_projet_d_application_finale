module MEM
  (
    input logic clk,
    input logic reset_n,
    input logic [31:0] OUT_EX,
    input logic [4:0] RD_add_EX,
    input logic Load_RD_EX,
    input logic Load_MEM_EX,
    input logic Write_MEM_EX,
    input logic [31:0] RD_RAM_EX,
    output logic [4:0] RD_add_MEM,
    output logic [31:0] OUT_MEM,
    output logic Load_RD_MEM,
    output logic Load_MEM_MEM,
    output logic Write_MEM_MEM,
    output logic [31:0] RD_RAM_MEM
    );

    always @ (posedge clk or negedge reset_n)
    begin
        if (!reset_n)
        begin
          RD_add_MEM <= 0 ;
          OUT_MEM <= 0 ;
          Load_RD_MEM <= 0 ;
          Load_MEM_MEM <= 0 ;
          Write_MEM_MEM <= 0 ;
          RD_RAM_MEM <= 0 ;
        end
        else
        begin
          RD_add_MEM <= RD_add_EX ;
          OUT_MEM <= OUT_EX ;
          Load_RD_MEM <= Load_RD_EX ;
          Load_MEM_MEM <= Load_MEM_EX ;
          Write_MEM_MEM <= Write_MEM_EX ;
          RD_RAM_MEM <= RD_RAM_EX ;
        end

    end



endmodule // MEM
