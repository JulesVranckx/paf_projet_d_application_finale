module WB
  (
    input logic clk,
    input logic reset_n,
    input logic [31:0] d_data_read,
    input logic [31:0] OUT_MEM,
    input logic [4:0] RD_add_MEM,
    input logic Load_RD_MEM,
    input logic Load_MEM_MEM,
    output logic [4:0] RD_add,
    output logic Load_RD,
    output logic [31:0] Value
    );

    logic [31:0] OUT;
    logic Load_MEM;

    always @ (posedge clk or negedge reset_n) begin
        if(!reset_n)
        begin
            RD_add <= 0;
            Load_RD <= 0;
            OUT <= 0;
            Load_MEM <= 0;
        end
        else
        begin
            RD_add <= RD_add_MEM;
            Load_RD <= Load_RD_MEM;
            OUT <= OUT_MEM;
            Load_MEM <= Load_MEM_MEM;
        end
    end

    always @ ( * ) begin
        if(Load_MEM)
            Value <= d_data_read;
        else
            Value <= OUT;
    end

endmodule // WB
