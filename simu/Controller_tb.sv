module Controller_tb ();

    logic clk;
    logic reset_n;
    logic [31:0] ALU_Out;
    logic [5:0] Op_code;
    logic Load_I;
    logic Inc_PC;
    logic Load_PC;
    logic Store_PC;
    logic Load_MEM;
    logic Load_Rd;
    logic Add_PC;
    logic Write_MEM;

    Controller controller_test( .clk(clk), .reset_n(reset_n), .ALU_Out(ALU_Out), .Op_code(Op_code), .Load_I(Load_I),
                                .R_Instr(R_Instr), .Inc_PC(Inc_PC), .Load_PC(Load_PC), .Store_PC(Store_PC),
                                .Load_MEM(Load_MEM), .Load_Rd(Load_Rd), .Add_PC(Add_PC), .Write_MEM(Write_MEM));

    always
    begin
        # 5ns
        clk <= !clk;
    end

    initial
    begin
        
        ALU_Out <= 0;
        clk <= 0;
        reset_n <= 0;
        # 20ns
        reset_n <= 1;
        # 90ns
        reset_n <= 0;
    end

endmodule // Controller_tb
