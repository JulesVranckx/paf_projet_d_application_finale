module GPIO
          (
            input logic clk,
            input logic reset_n,
            input logic [31:0] address,
            input logic [31:0] data_write,
            input logic write_enable,
            input logic [9:0] sw,
            output logic [31:0] data_read
            output logic [9:0] ledr
            );

            // On adresse 0 pour lire l'etat de sw, et 4 pour agir sur les LEDs


            // Registre contenant l'etat des LEDs a maintenir
            logic [9:0] led_state ;

            always @(posedge clk or negedge reset_n)
                if (!reset_n)
                  begin
                  ledr <= 0 ;
                  end
                else
                  ledr <= led_state ;


            always @(*)
              if (address == 0)
                data_read <= sw ;
              else if (address == 4)
                    begin
                    if (write_enable)
                      led_state <= data_write ;
                    data_read <= ledr ;
                    end

endmodule // GPIO
