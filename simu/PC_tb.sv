module PC_tb ();
    logic clk;
    logic reset_n;
    logic Add_PC;
    logic Inc_PC;
    logic Load_PC;
    logic [31:0] RS1;
    logic [31:0] ALU_Out;
    logic [31:0] Value;

    PC pc_test(.clk(clk), .reset_n(reset_n), .Add_PC(Add_PC), .Inc_PC(Inc_PC), .Load_PC(Load_PC), .RS1(RS1), .ALU_Out(ALU_Out), .Value(Value));

    always begin
        # 5ns
        clk <= !clk;
    end

    initial begin
        clk <= 0;
        reset_n <= 0;
        Add_PC <= 0;
        Inc_PC <= 0;
        Load_PC <= 0;
        RS1 <= 60;
        ALU_Out <= 0;
        # 5ns
        reset_n <= 1;
        Inc_PC <= 1;
        # 50ns
        Load_PC <= 1;
        # 10ns
        Load_PC <= 0;
        # 50ns
        reset_n <= 0;
        # 20ns
        reset_n <= 1;
    end

endmodule // PC_tb
