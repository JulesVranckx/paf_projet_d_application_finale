module banc_de_registres(
													input logic clk,
													input logic  reset_n,
													input logic [4:0] RS1_add,
													input logic [4:0] RS2_add,
													input logic [4:0] RD_add,
													input logic [4:0] RD_in_add,
													input logic Load_RD,
													input logic Store_PC,
													input logic [31:0] Value,
													input logic [31:0] PC,
													output logic [31:0] RS1,
													output logic [31:0] RS2,
													output logic [31:0] RD);

		logic [31:0] regs [31:0];


        always @(posedge clk or negedge reset_n)
            if (!reset_n)
            begin
                for(integer i=0;i<32;i=i+1) begin
									regs[i] <= 0;
								end
            end
            else
            begin
								if (Store_PC)
										regs[31] <= PC + 4 ;
                if(Load_RD && (RD_in_add != 0))
										regs[RD_in_add] <= Value ;
            end

		always @(posedge clk or negedge reset_n)
			if (!reset_n)
				begin
					RS1 <= 0 ;
					RS2 <= 0 ;
					RD <= 0 ;
					end
			else
				begin
					if(RS1_add == RD_in_add)
						RS1 <= Value;
					else
						RS1 <= regs[RS1_add];

					if(RS2_add == RD_in_add)
						RS2 <= Value;
					else
						RS2 <= regs[RS2_add];

					if(RD_add == RD_in_add)
						RD <= Value;
					else
						RD <= regs[RD_add];
				end

endmodule
