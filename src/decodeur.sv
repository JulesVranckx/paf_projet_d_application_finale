module decodeur
        (
        input logic clk,
        input logic reset_n,
        input logic [31:0] i_data,
        output logic [5:0] Op_code,
        output logic [31:0] Immediate,
        output logic [4:0] RS1_add,
        output logic [4:0] RS2_add,
        output logic [4:0] RD_add,
        output logic Add_PC,
        output logic Store_PC,
        output logic Load_MEM,
        output logic Load_RD,
        output logic Write_MEM,
				output logic R_instr
        );

        // Définition du jeu d'instructions
        localparam ADD = 6'h20;
        localparam ADDI = 6'h08;
        localparam AND = 6'h24;
        localparam ANDI = 6'h0c;
        localparam BEQZ = 6'h04;
        localparam BNEZ = 6'h05;
        localparam J = 6'h02;
        localparam JAL = 6'h03;
        localparam JALR = 6'h13;
        localparam JR = 6'h12;
        localparam LHI = 6'h0f;
        localparam LW = 6'h23;
        localparam OR = 6'h25;
        localparam ORI = 6'h0d;
        localparam SEQ = 6'h28;
        localparam SEQI = 6'h18;
        localparam SLE = 6'h2c;
        localparam SLEI = 6'h1c;
        localparam SLL = 6'h04;
        localparam SLLI = 6'h14;
        localparam SLT = 6'h2a;
        localparam SLTI = 6'h1a;
        localparam SNE = 6'h29;
        localparam SNEI = 6'h19;
        localparam SRA = 6'h07;
        localparam SRAI = 6'h17;
        localparam SRL = 6'h06;
        localparam SRLI = 6'h16;
        localparam SUB = 6'h22;
        localparam SUBI = 6'h0a;
        localparam SW = 6'h2b;
        localparam XOR = 6'h26;
        localparam XORI = 6'h0e;



        enum logic [1:0] {R,I_instr,J_instr} data_type; //instruction type

        always @(*)

          if (i_data[31:26] == 0)
            data_type <= R ;
          else if (i_data[31:26] ==  J || i_data[31:26] == JAL)
                  data_type <= J_instr ;
               else
                  data_type <= I_instr ;

        always @(*)

          case (data_type)

            R : begin

                RS1_add <= i_data[25:21] ;
                RS2_add <= i_data[20:16] ;
                RD_add <= i_data[15:11] ;
                Op_code <= i_data[5:0] ;
                Immediate <= 0 ;

                end

            I_instr : begin

                RS1_add <= i_data[25:21] ;
                RS2_add <= 0 ;
                RD_add <= i_data[20:16] ;
                Op_code <= i_data[31:26] ;

                if ( (Op_code <= LHI && Op_code >= ANDI) || Op_code == SLLI || Op_code == SRLI || Op_code == SRAI)

                  Immediate <= i_data[15:0] ;  //extend

                else

                  Immediate <= { {16{i_data[15]}}, i_data[15:0]};


                end

            J_instr : begin

                RS1_add <= 0;
                RS2_add <= 0 ;
                RD_add <= 0 ;
                Op_code <= i_data[31:26] ;
                Immediate <= { {6{i_data[25]}} , i_data[25:0]} ;

                end

          endcase

        always @ ( * )
        begin
          Load_RD <= (Op_code!=JAL) && (Op_code!=JALR) && (Op_code!=BEQZ) && (Op_code!=BNEZ) && (Op_code!=J)
                      && (Op_code!=JR) && (Op_code!=SW);
          Store_PC <= (Op_code==JAL) || (Op_code==JALR);
          Load_MEM <= (Op_code==LW);
          Write_MEM <= (Op_code==SW);
          Add_PC <= (Op_code==BEQZ) || (Op_code==BNEZ) || (Op_code==J) || (Op_code==JAL);
        end

				always @(*)
					R_instr <= (data_type == R);

endmodule //decodeur
