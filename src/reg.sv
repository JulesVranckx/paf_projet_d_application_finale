module registre(input logic clk,
		input logic  reset_n,
		input logic [31:0]  e,
		input logic  ena,
		output logic [31:0] s);

   always @(posedge clk or negedge reset_n)
     if (~reset_n)
       s <= 0;
     else
       if (ena)
	      s <= e;
endmodule // reg
