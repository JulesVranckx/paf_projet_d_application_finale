module ID
    (
      input logic clk,
      input logic reset_n,
      input logic [31:0] i_data,
      input logic [31:0] i_addr_IF,
      output logic [4:0] RS1_add,
      output logic [4:0] RS2_add,
      output logic [4:0] RD_add_ID,
      output logic [5:0] Op_code_ID,
      output logic [31:0] Immediate_ID,
      output logic Load_RD_ID,
      output logic Add_PC_ID,
      output logic Store_PC,
      output logic Load_MEM_ID,
      output logic Write_MEM_ID,
      output logic [31:0] i_addr_ID,
			output logic R_instr
      );

      decodeur decodeur(
                        .clk(clk),
                        .reset_n(reset_n),
                        .i_data(i_data),
                        .Op_code(Op_code_ID),
                        .Immediate(Immediate_ID),
                        .RS1_add(RS1_add),
                        .RS2_add(RS2_add),
                        .RD_add(RD_add_ID),
                        .Add_PC(Add_PC_ID),
                        .Load_MEM(Load_MEM_ID),
                        .Load_RD(Load_RD_ID),
                        .Store_PC(Store_PC),
                        .Write_MEM(Write_MEM_ID),
												.R_instr(R_instr)
                        );
      always @ (posedge clk or negedge reset_n)
      begin
          if (!reset_n)
            i_addr_ID <= 0 ;
          else
            i_addr_ID <= i_addr_IF ;
      end

endmodule // ID
