#########################################################################################################################
#########################################################################################################################
################################### Transformation assembleur en langage machine ########################################
#########################################################################################################################
#########################################################################################################################

import sys

#########################################################################################################################
################################### Ouverture du fichier ################################################################
#########################################################################################################################
cmd = sys.argv
data = cmd[1]

######################Open file########################
def open_file(data):
	"""Ouvre le ficher 'data' et renvoie un tableau de tableau, comprenant chacun une ligne de code, ainsi que le nombre de lignes"""
	lines = ""
	code = []

	with open(data) as f :

		for data_line in f:
			lines = lines + data_line
	lines = lines.split("\n")
	for line in lines :
		temp = line.split(" ").split(",")
		code.append(temp)
	code.remove([""])
	return code
########################################################

#########################################################################################################################
################################### Convertion en binaire ###############################################################
#########################################################################################################################

def sign_extented(data, n):
	"""Etend data sur n bits, de maniere signee si data[0] = -1"""
	if data[0] == "-":
		data_temp1 = data[1:]
		data_temp1 = bin(int(data_temp1))[2:].zfill(n)
		data_temp = ""
		for i in range(0,len(data_temp1)):
			data_temp = data_temp + str(int(data_temp1[i]) ^ 1)
		data_temp = bin(int(data_temp,2) + 1)[2:]
	else :
		data_temp = bin(int(data))[2:].zfill(n)
	return data_temp



######################Decrypt R#########################
def decrypt_R_b(line,end):
	"""Decrypte une instruction de type R"""
	ret = "000000"
	if 0 <= int(line[1][1:]) <= 31 and 0 <= int(line[2][1:]) <= 31  and 0 <= int(line[3][1:]) <= 31:
		ret += bin(int(line[1][1:]))[2:].zfill(5)
		ret += bin(int(line[2][1:]))[2:].zfill(5)
		ret += bin(int(line[3][1:]))[2:].zfill(5)
		ret += "00000"
		ret += end
		ret += "\n"
	else :
		print("Valeur de registre incorrecte")
		sys.exit(-1)
	return ret
#######################################################

###################### decrypt_I ########################
def decrypt_I_b(line, deb):
	"""Decrypte une instruction de type I"""
	ret = deb
	if 0 <= int(line[1][1:]) <= 31 and 0 <= int(line[2][1:]) <= 31 :
		ret += bin(int(line[1][1:]))[2:].zfill(5)
		ret += bin(int(line[2][1:]))[2:].zfill(5)
		ret += sign_extented(line[3],16)
		ret = ret + "\n"
	else :
		print("Valeur de registre incorrecte")
		sys.exit(-1)
	return ret
#######################################################

###################### decrypt_I_without_RD ########################
def decrypt_I_without_RD_b(line, deb):
	"""Decrypte une instruction de type I"""
	ret = deb
	if 0 <= int(line[1][1:]) <= 31:
		ret = ret + bin(int(line[1][1:]))[2:].zfill(5)
		ret = ret + "00000"
		ret = ret + sign_extented(line[2],16)
		ret = ret + "\n"
	else :
		print("Valeur de registre incorrecte")
		sys.exit(-1)
	return ret
#######################################################

###################### decrypt_I_without_RD_and_Immediate ########################
def decrypt_I_without_RD_and_Immediate_b(line, deb):
	"""Decrypte une instruction de type I"""
	ret = deb
	if 0 <= int(line[1][1:]) <= 31:
		ret = ret + bin(int(line[1][1:]))[2:].zfill(5)
		ret = ret + "00000"
		ret = ret + "0000000000000000"ORI $1 $0 1
		ret = ret + "\n"
	else :
		print("Valeur de registre incorrecte")
		sys.exit(-1)
	return ret
#######################################################

###################### decrypt_J ########################
def decrypt_J_b(line, deb):
	"""Decrypte une instruction de type I"""
	ret = deb
	ret = ret + sign_extented(line[1], 26)
	ret = ret + "\n"
	return ret
#######################################################


###################### binaire ########################
def binaire(code):
	"""Convertit code en binaire et un entier en binaire"""
	ret = ""

	for line in code :
		if line[0] == "ADD" :
			ret = ret + decrypt_R_b(line, "100000")
		elif line[0] == "ADDI" :
			ret = ret + decrypt_I_b(line, "001000")
		elif line[0] == "AND" :
			ret = ret + decrypt_R_b(line, "100100")
		elif line[0] == "ANDI" :
			ret = ret + decrypt_I_b(line, "001100")
		elif line[0] == "BEQZ" :
			ret = ret + decrypt_I_without_RD_b(line, "000100")
		elif line[0] == "BNEZ" :
			ret = ret + decrypt_I_without_RD_b(line, "000101")
		elif line[0] == "J" :
			ret = ret + decrypt_J_b(line, "000010")
		elif line[0] == "JAL" :
			ret = ret + decrypt_J_b(line, "000011")
		elif line[0] == "JALR" :
			ret = ret + decrypt_I_without_RD_b(line, "010011")
		elif line[0] == "JR" :
			ret = ret + decrypt_I_without_RD_and_Immediate_b(line, "010010")
		elif line[0] == "LHI" :
			ret = ret + decrypt_I_b(line, "001111")
		elif line[0] == "LW" :
			ret = ret + decrypt_I_b(line, "100011")
		elif line[0] == "OR" :
			ret = ret + decrypt_R_b(line, "100101")
		elif line[0] == "ORI" :
			ret = ret + decrypt_I_b(line, "001101")
		elif line[0] == "SEQ" :
			ret = ret + decrypt_R_b(line, "101000")
		elif line[0] == "SEQI" :
			ret = ret + decrypt_I_b(line, "011000")
		elif line[0] == "SLE" :
			ret = ret + decrypt_R_b(line, "101100")
		elif line[0] == "SLEI" :
			ret = ret + decrypt_I_b(line, "011100")
		elif line[0] == "SLL" :
			ret = ret + decrypt_R_b(line, "000100")
		elif line[0] == "SLLI" :
			ret = ret + decrypt_I_b(line, "010100")
		elif line[0] == "SLT" :
			ret = ret + decrypt_R_b(line, "101010")
		elif line[0] == "SLTI" :
			ret = ret + decrypt_I_b(line, "011010")
		elif line[0] == "SNE" :
			ret = ret + decrypt_R_b(line, "101001")
		elif line[0] == "SNEI" :
			ret = ret + decrypt_I_b(line, "011001")
		elif line[0] == "SRA" :
			ret = ret + decrypt_R_b(line, "000111")
		elif line[0] == "SRAI" :
			ret = ret + decrypt_I_b(line, "010111")
		elif line[0] == "SRL" :
			ret = ret + decrypt_R_b(line, "000110")
		elif line[0] == "SRLI" :
			ret = ret + decrypt_I_b(line, "010110")
		elif line[0] == "SUB" :
			ret = ret + decrypt_R_b(line, "100010")
		elif line[0] == "SUBI" :
			ret = ret + decrypt_I_b(line, "001010")
		elif line[0] == "SW" :
			ret = ret + decrypt_I_b(line, "101100")
		elif line[0] == "XOR" :
			ret = ret + decrypt_R_b(line, "100110")
		elif line[0] == "XORI" :
			ret = ret + decrypt_I_b(line, "001110")
		else :
			print("Fatal Error :\nUnknown instruction")
			return None

	return ret
#######################################################



def hexa(code):
	temp = binaire(code)
	temp = temp.split("\n")
	temp = temp[:-1]
	ret = ""
	for line in temp :
		ret += hex(int(line,2))[2:].zfill(8)
		print(line)
		ret += "\n"
	return ret

print(hexa(open_file(data)))
