module Controller_tb_fibo ();

    logic [31:0] I;
    logic clk;
    logic reset_n;
    logic ALU_Out;
    logic Load_I;
    logic R_Instr;
    logic Inc_PC;
    logic Load_PC;
    logic Store_PC;
    logic Load_MEM;
    logic Load_Rd;
    logic Add_PC;
    logic Write_MEM;

    Controller controller_fibo( .I(I), .clk(clk), .reset_n(reset_n), .ALU_Out(ALU_Out), .Load_I(Load_I),
                                .R_Instr(R_Instr), .Inc_PC(Inc_PC), .Load_PC(Load_PC), .Store_PC(Store_PC),
                                .Load_MEM(Load_MEM), .Load_Rd(Load_Rd), .Add_PC(Add_PC), .Write_MEM(Write_MEM));

    always
    begin
        # 5ns
        clk <= !clk;
    end

    initial
    begin
        I <= 32'b00001000000000000000000000000000;
        ALU_Out <= 0;
        clk <= 0;
        reset_n <= 0;
        # 20ns
        reset_n <= 1;

                                                    // debut:
        I <= 32'b00110100000000010000000000000001;  // ORI  $1,$0,1
        # 50ns
        I <= 32'b00000000000000010001000000100101;  // OR   $2,$0,$1
        # 50ns
        I <= 32'b00110100000000110000000000001010;  // ORI  $3,$0,10
        # 50ns
                                                    // boucle:
        I <= 32'b00000000001000100010000000100000;  // ADD  $4,$1,$2
        # 50ns
        I <= 32'b00000000000000100000100000100101;  // OR   $1,$0,$2
        # 50ns
        I <= 32'b00000000000001000001000000100101;  // OR   $2,$0,$4
        # 50ns
        I <= 32'b00100000011000111111111111111111;  // ADDI $3,$3,-1
        # 50ns
        I <= 32'b00010100011000001111111111110000;  // BNEZ $3,boucle
        # 50ns
                                                    // fin:
        I <= 32'b00001000000000000000000000000000;  // J    fin


    end

endmodule // Controller_tb
