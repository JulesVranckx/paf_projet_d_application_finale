module DLX
  (
   input logic 	       clk,
   input logic 	       reset_n,

   // RAM contenant les données
   output logic [31:0] d_address,
   input logic [31:0]  d_data_read,
   output logic [31:0] d_data_write,
   output logic        d_write_enable,
   input logic 	       d_data_valid,

   // ROM contenant les instructions
   output logic [31:0] i_address,
   input logic [31:0]  i_data_read,
   input logic 	       i_data_valid
   );

    // IF


    // ID
    logic [4:0] RD_add_ID;
    logic [5:0] Op_code_ID;
    logic [31:0] Immediate_ID;
    logic Load_RD_ID;
    logic Add_PC_ID;
    logic Load_MEM_ID;
    logic Write_MEM_ID;
    logic [31:0] i_addr_ID;
		logic R_instr;


    // banc_de_registres
    logic [4:0] RS1_add;
    logic [4:0] RS2_add;

    logic [4:0] RD_in_add;
    logic Load_RD;
    logic [31:0] Value;

    logic Store_PC;
    logic [31:0] RS1;
    logic [31:0] RS2;
    logic [31:0] RD;

    // EX
    logic [31:0] OUT_EX;
    logic [4:0] RD_add_EX;
    logic Load_RD_EX;
    logic Load_MEM_EX;
    logic Write_MEM_EX;
    logic [31:0] RD_RAM_EX;
    logic Add_PC_EX;
    logic Load_PC;
    logic [31:0] i_addr_EX;

    // MEM
    logic [4:0] RD_add_MEM;
    logic Load_RD_MEM;
    logic Load_MEM_MEM;

    // WB
    //


    IF IF(  .clk(clk), .reset_n(reset_n), .Add_PC(Add_PC_EX), .Load_PC(Load_PC), .RS1(RS1), .ALU_Out(OUT_EX), .PC(i_address), .i_addr_EX(i_addr_EX));
    ID ID(  .clk(clk), .reset_n(reset_n), .i_data(i_data_read), .i_addr_IF(i_address), .RS1_add(RS1_add), .RS2_add(RS2_add), .RD_add_ID(RD_add_ID),
            .Op_code_ID(Op_code_ID), .Immediate_ID(Immediate_ID), .Load_RD_ID(Load_RD_ID), .Add_PC_ID(Add_PC_ID),
            .Store_PC(Store_PC), .Load_MEM_ID(Load_MEM_ID), .Write_MEM_ID(Write_MEM_ID), .i_addr_ID(i_addr_ID), .R_instr(R_instr));


    banc_de_registres regs(  .clk(clk), .reset_n(reset_n), .RS1_add(RS1_add), .RS2_add(RS2_add),
                            .RD_add(RD_add_ID), .RD_in_add(RD_in_add), .Load_RD(Load_RD), .Store_PC(Store_PC),
                            .Value(Value), .PC(i_addr_ID), .RS1(RS1), .RS2(RS2), .RD(RD));

    EX EX(  .clk(clk), .reset_n(reset_n), .RS1(RS1), .RS2(RS2), .RD(RD), .Immediate_ID(Immediate_ID), .Op_code_ID(Op_code_ID),
            .RD_add_ID(RD_add_ID), .Add_PC_ID(Add_PC_ID), .OUT_MEM(d_address), .OUT_WB(Value), .RD_add_MEM(RD_add_MEM), .R_instr(R_instr), .RD_in_add(RD_in_add),
            .Load_RD_MEM(Load_RD_MEM), .Load_RD_WB(Load_RD), .RS1_add(RS1_add), .RS2_add(RS2_add), .Load_RD_ID(Load_RD_ID), .Load_MEM_ID(Load_MEM_ID), .Write_MEM_ID(Write_MEM_ID),
            .OUT_EX(OUT_EX), .RD_add_EX(RD_add_EX), .Load_RD_EX(Load_RD_EX), .Load_MEM_EX(Load_MEM_EX), .Write_MEM_EX(Write_MEM_EX),
            .RD_RAM_EX(RD_RAM_EX), .Add_PC_EX(Add_PC_EX), .Load_PC(Load_PC), .i_addr_ID(i_addr_ID), .i_addr_EX(i_addr_EX));
    MEM MEM(  .clk(clk), .reset_n(reset_n), .OUT_EX(OUT_EX), .RD_add_EX(RD_add_EX), .Load_RD_EX(Load_RD_EX), .Load_MEM_EX(Load_MEM_EX),
              .Write_MEM_EX(Write_MEM_EX), .RD_RAM_EX(RD_RAM_EX), .RD_add_MEM(RD_add_MEM), .OUT_MEM(d_address), .Load_RD_MEM(Load_RD_MEM),
              .Load_MEM_MEM(Load_MEM_MEM), .Write_MEM_MEM(d_write_enable), .RD_RAM_MEM(d_data_write));
    WB WB(    .clk(clk), .reset_n(reset_n), .d_data_read(d_data_read), .OUT_MEM(d_address), .RD_add_MEM(RD_add_MEM),
              .Load_RD_MEM(Load_RD_MEM), .Load_MEM_MEM(Load_MEM_MEM), .RD_add(RD_in_add), .Load_RD(Load_RD), .Value(Value));



endmodule // DLX
