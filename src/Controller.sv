module Controller(  input logic clk,
                    input logic reset_n,
                    input logic [31:0] ALU_Out,
                    input logic [5:0] Op_code,
                    output logic Load_I,
                    output logic Inc_PC,
                    output logic Load_PC,
                    output logic Store_PC,
                    output logic Load_MEM,
                    output logic Load_Rd,
                    output logic Add_PC,
                    output logic Write_MEM
                    );

    // Définition du jeu d'instructions
    localparam ADD = 6'h20;
    localparam ADDI = 6'h08;
    localparam AND = 6'h24;
    localparam ANDI = 6'h0c;
    localparam BEQZ = 6'h04;
    localparam BNEZ = 6'h05;
    localparam J = 6'h02;
    localparam JAL = 6'h03;
    localparam JALR = 6'h13;
    localparam JR = 6'h12;
    localparam LHI = 6'h0f;
    localparam LW = 6'h23;
    localparam OR = 6'h25;
    localparam ORI = 6'h0d;
    localparam SEQ = 6'h28;
    localparam SEQI = 6'h18;
    localparam SLE = 6'h2c;
    localparam SLEI = 6'h1c;
    localparam SLL = 6'h04;
    localparam SLLI = 6'h14;
    localparam SLT = 6'h2a;
    localparam SLTI = 6'h1a;
    localparam SNE = 6'h29;
    localparam SNEI = 6'h19;
    localparam SRA = 6'h07;
    localparam SRAI = 6'h17;
    localparam SRL = 6'h06;
    localparam SRLI = 6'h16;
    localparam SUB = 6'h22;
    localparam SUBI = 6'h0a;
    localparam SW = 6'h2b;
    localparam XOR = 6'h26;
    localparam XORI = 6'h0e;

    enum logic [2:0] {IF, ID, EX, MEM, WB} state, n_state;

    always @ (posedge clk or negedge reset_n) begin
        if(!reset_n)
            begin
                state <= IF;
                n_state <= ID;
            end
        else
            begin
                state <= n_state;
            end
    end

    always @(*) begin
        case(state)
            IF: n_state <= ID;
            ID: n_state <= EX;
            EX: n_state <= MEM;
            MEM: n_state <= WB;
            WB: n_state <= IF;
        endcase
    end

    always @ ( * ) begin
        Load_I <= (state==ID);
        Inc_PC <= (state==WB) && ((Op_code!=BEQZ || ALU_Out!=0) && (Op_code!=BNEZ || ALU_Out==0) && (Op_code!=J) && (Op_code!=JAL) && (Op_code!=JALR) && (Op_code!=JR));
        Load_PC <= (state==WB) && ((Op_code==BEQZ && ALU_Out==0) || (Op_code==BNEZ && ALU_Out!=0) || (Op_code==J) || (Op_code==JAL) || (Op_code==JALR) || (Op_code==JR));
        Store_PC <= (state==WB) && ((Op_code==JAL) || (Op_code==JALR));
        Load_MEM <= (state==WB) && (Op_code==LW);
        Load_Rd <= (state==WB) && ((Op_code!=BEQZ) && (Op_code!=BNEZ) && (Op_code!=J) && (Op_code!=JR) && (Op_code!=SW));
        Add_PC <= (state==WB) && ((Op_code==BEQZ) || (Op_code==BNEZ) || (Op_code==J) || (Op_code==JAL));
        Write_MEM <= (state==MEM) && (Op_code==SW);
    end



endmodule
