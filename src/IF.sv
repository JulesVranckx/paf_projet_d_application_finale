module IF ( input logic clk,
            input logic reset_n,
            input logic Add_PC,
            input logic Load_PC,
            input logic [31:0] RS1,
            input logic signed [31:0] ALU_Out,
            input logic [31:0] i_addr_EX,
            output logic [31:0] PC);

    logic [31:0] i_addr_IF;

    always @ (posedge clk or negedge reset_n) begin
        if(!reset_n)
            i_addr_IF <= 0;
        else begin
            if (Load_PC) begin
                if(Add_PC)
                    i_addr_IF <= i_addr_EX + ALU_Out;
                else
                    i_addr_IF <= RS1;
            end
            else begin
                i_addr_IF <= i_addr_IF + 4;
            end
        end
    end

    always @ ( * ) begin
        if (Load_PC) begin
            if(Add_PC)
                PC <= i_addr_EX + ALU_Out;
            else
                PC <= RS1;
        end else begin
            PC <= i_addr_IF;
        end
    end

endmodule // IF
