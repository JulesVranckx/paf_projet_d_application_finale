module ALU_tb ();


  logic [31:0] RS1;
  logic [31:0] RS2;
  logic [31:0] Immediate;
  logic [5:0] Op_code;
  logic [31:0] OUT;


  ALU ALU(.RS1(RS1), .RS2(RS2), .Immediate(Immediate), .Op_code(Op_code), .OUT(OUT));

	logic [5:0] index;

  initial begin
		RS1 <= 32'd1 ;
		RS2 <= 32'd2 ;
		Immediate <= 0 ;
		Op_code <= 6'h20 ;

    for (index = 0; index < 32; index = index + 1)
      begin

        #5ns begin

                RS1 <= RS1 + 32'd12750;
                RS2 <= RS2 + 32'd15681;
                Immediate <= Immediate + 32'd1568;
                Op_code <= Op_code + 7;
             end
      end
	end


endmodule //ALU_tb
